def setup():
    size(500,500)
    background(0)
    translate(250,250)
    c = 400
    fill (random(255), random(255), random(255))
    square(-c/2, -c/2, c)
    c = sqrt(c*c /2)
    rotate(PI/4)
    square(-c/2, -c/2, c)
    i = 10
    while i>0:
        fill (random(255), random(255), random(255))
        square(-c/2, -c/2, c)
        c = sqrt(c*c /2)
        rotate(PI/4)
        square(-c/2, -c/2, c)
        i = i-1
