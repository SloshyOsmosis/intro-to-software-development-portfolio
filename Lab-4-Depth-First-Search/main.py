graph = {"1":["2","4"], 
"2":["1","3","5","7","8"], 
"3":["9","10"], 
"4":["3"], 
"5":["6","7","8"], 
"6":[], 
"7":["8"], 
"8":["5","7"], 
"9":[], 
"10":[]}

print(graph)
visitednode = set()

def dfs(visitednode, graph, rootnode): #checking starting point, also have to check if child nodes have been visited
    if rootnode not in visitednode:
        print(rootnode)
        visitednode.add(rootnode)
        for childnode in graph[rootnode]:
            dfs(visitednode, graph, childnode)#rootnode and childnode are placeholders, not defined

print("DFS:")
dfs(visitednode, graph, "1")