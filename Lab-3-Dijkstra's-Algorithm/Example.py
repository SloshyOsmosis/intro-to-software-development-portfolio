
import matplotlib.pyplot as plt
import networkx as nx

import matplotlib.pyplot as plt
import networkx as nx
# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


#Modify so as to link Paris to Dover with weight of 1; Belfast to Dublin weight 0.1; London to Norwhich with a weight of 1
#Think of how to position each of those cities on the graph


G = nx.Graph()
G.graph["Name"] = "Demo My Basic Map"
#
#Nodes with attributes using dictionnary data structure

G.add_edges_from([
    ("A", "B", {"Weight": 2}),
    ("A", "D", {"Weight": 8}),
    ("B", "D", {"Weight": 5}),
    ("B", "E", {"Weight": 6}),
    ("D", "E", {"Weight": 3}),
    ("D", "F", {"Weight": 2}),
    ("E", "F", {"Weight": 1}),
    ("E", "C", {"Weight": 9}),
    ("F", "C", {"Weight": 3})

])
pos={
    "A":(0,0),
    "B":(15, 15),
    "C":(45, 0),
    "D":(15, -15),
    "E":(30, 15),
    "F":(30, -15)

}
#Using for loop

#pos_node_attributes ={}
#add value to dictionnary
# for loop for what is above
#node & corresponding coordinates and add value
#for node, (x, y) in pos.items():
    #pos_node_attributes[node] = (x, y - 0.9)
    #from below node name etc..
    #the below is no longer required
'''''
#the below is hand typed; the above is better with a loop
pos_node_attributes ={
    "A":(-0.2, 5),#Positions of lable node A
    "B":(4.5, 7.5),
    "C":(2.4, 1.4),
    "D":(5.8, 2.5),
    "E":(9.1, 3.6)
}
'''''
#Create a dictionnary for a key value pairs in this case
#node_labels = {n:(d["Population"],d["Settlement"]) for n, d in G.nodes(data=True)}

#create a dictionary for this

edge_labels = {(u, v):d["Weight"] for u,v,d in G.edges(data=True)}

nx.draw(G, pos=pos, with_labels=True,
        node_color = "blue", node_size = 3000,
        font_color = "white", font_size = 20, font_family = "Arial", font_weight = "bold",
        edge_color = "black",
        width = 5
)
#Added 3rd version to display weight to draw the edge labels
nx.draw_networkx_edge_labels(G, pos=pos, edge_labels=edge_labels, label_pos=0.5)

#nx.draw_networkx_labels(G, pos=pos_node_attributes, 
        #font_color = "black", font_size = 10, font_family = "Times New Roman", font_weight = "bold")

plt.margins(0.2)
plt.show()