import matplotlib.pyplot as plt
import networkx as nx

G = nx.Graph()
G.graph["Name"] = "Population and Cities"

G.add_nodes_from([
    ("Leeds", {"Population":"800000", "Settlement":"City"}),
    ("Glasgow", {"Population":"300000", "Settlement":"City"}),
    ("Birmingham", {"Population":"200000", "Settlement":"City"}),
    ("London", {"Population":"10 Million", "Settlement":"City"}),
    ("Dover", {"Population":"100000", "Settlement":"Town"}),
    ("Cardiff",{"Population":"200000", "Settlement":"City"}),
    ("Belfast",{"Population":"200000", "Settlement":"City"}),
    ("Paris",{"Population":"200000", "Settlement":"City"}),
    ("Dublin",{"Population":"200000", "Settlement":"City"}),
    ("Norwhich",{"Population":"200000", "Settlement":"City"})
])

G.add_edges_from([
    ("Leeds", "Birmingham", {"Weight": 0.1}),
    ("Leeds", "Belfast", {"Weight": 0.1}),
    ("Leeds", "Glasgow", {"Weight": 0.1}),
    ("Glasgow", "Birmingham", {"Weight":0.2}),
    ("Glasgow", "London", {"Weight":0.6}),
    ("Birmingham", "London",{"Weight":1}),
    ("London", "Dover",{"Weight": 1}),
    ("Cardiff", "Birmingham", {"Weight":1}),
    ("Cardiff", "London", {"Weight":1}),
    ("Cardiff", "Belfast", {"Weight":0.1}),
    ("Belfast", "Dublin", {"Weight":0.1}),
    ("Paris", "Dover",{"Weight":1}),
    ("London", "Norwhich",{"Weight":1})
])

pos={
    "Leeds":(0,19.6),
    "Glasgow":(32.5, 24.6),
    "Birmingham":(10.6, 1.4),
    "London":(29.8, -9.5),
    "Dover":(49.9, -12),
    "Cardiff":(0, -17),
    "Belfast":(-19, -3),
    "Paris":(80, -20),
    "Dublin" : (-23, -20),
    "Norwhich" : (50, -30)
}

population = nx.get_node_attributes(G, "Population")
settlement = nx.get_node_attributes(G, "Settlement")
pos_node_attributes ={}

for node, (x, y) in pos.items():
    pos_node_attributes[node] = (x, y + 1)

edge_labels = {(u, v):d["Weight"] for u,v,d in G.edges(data=True)}

nx.draw(G, pos=pos, labels= population,
        node_color = "orange", node_size = 3000,
        font_color = "black", font_size = 10, font_family = "Times New Roman", font_weight = "bold",
        edge_color = "blue",
        width = 5
)

nx.draw_networkx_edge_labels(G, pos=pos, edge_labels=edge_labels, label_pos=0.5)

nx.draw_networkx_labels(G, pos=pos_node_attributes,
        font_color = "black", font_size = 10, font_family = "Arial", font_weight = "bold")

plt.margins(0.2)
plt.show()