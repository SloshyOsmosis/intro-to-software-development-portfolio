from random import randint, random
import pygame, sys, math
#Importing all of the modules

pygame.init() #Initialising PyGame

screen = pygame.display.set_mode((940,590)) #Creates the window and gives it a size in pixels
pygame.display.set_caption('DVD Screensaver') #Gives the window a name

def_color = (255,0,0) #Default color of the rectangle

x = randint(100,200)
y = randint(100,200) #Creates a random position for the rectangle
dx = 0.2
dy = -0.2 #Speed of the rectangle

while True: #Creates the loop for the game, without it the game would immediately shut down, while loops keep it running 'forever'
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
    
    screen.fill((0,0,0)) #Background color.

    r = randint(0,255)
    g = randint(0,255)
    b = randint(0,255) #Variables to create the random color 

    rgb = (r,g,b) #Random color variable
    x = x + dx
    y = y + dy #Adding the position of the rectangle to the speed, causing it to move around the screen.

    if x > 800: #Instances where the rectangle will change direction when hitting the wall
        dx *= -1
        def_color = rgb #Changes the color of the rectangle to a random color once it hits the side of the box. 
    elif x < 0:
        dx *= -1
        def_color = rgb 
    elif y > 500:
        dy *= -1
        def_color = rgb
    elif y < 0:
        dy *= -1
        def_color = rgb
    
    pygame.draw.rect(screen, def_color, pygame.Rect(x, y, 140, 90)) #Drawing the rectangle.
    pygame.display.flip()
    pygame.display.update() #Refreshing the screen to show the position of the rectangle.