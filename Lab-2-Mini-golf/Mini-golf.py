from random import randint
import sys, pygame, math

## Setup code

pygame.init()
pygame.font.init()

## Initialise all variables here

screen = pygame.display.set_mode((500,500)) #Setting the size of the screen, in this case it will be 500x500 pixels
x = randint(50,450)
y = randint(50,450) #Randomly placing the location of the golf ball
dx = 0
dy = 0 #Speed of the golf ball, 0 means the ball will be stationary.

ball_color = (255,255,255)
ball_color_win = (255,255,0) #Variables for ball colors.

goal_X = randint(20,480)
goal_y = randint(20,480) #Randomly placing the location of the hole

aiming = True
game_won = False #Default
# Game loop

while True:
    # Event detection
    # This is necessary for being able to close the game
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        if event.type == pygame.MOUSEBUTTONUP:
            pos = pygame.mouse.get_pos()
            mousex = pos[0]
            mousey = pos[1]
            k = 50
            c1 = mousex - x
            c2 = mousey - y 
            dx = c1 / k
            dy = c2 / k
            x1 = x - goal_X
            y1 = y - goal_y
            dist = int(math.sqrt(c1*c1 + c2*c2))
            goal_dist = int(math.sqrt(x1*x1 + y1*y1))
            print("Clicked distance from the ball is",dist,"pixels") #Determines distance clicked from player.
            print("Distance from the Goal is",goal_dist,"pixels") #Determines distance clicked from goal.
            aiming = False

    if aiming == False: #If False will cause the ball to move
        x = x + dx
        y = y + dy

    x1 = x - goal_X
    y1 = y - goal_y
    goal_dist = int(math.sqrt(x1*x1 + y1*y1))#Calculating distance from the ball, taken from the MOUSEBUTTONUP event.

    if goal_dist <= 10: #Winning condition
        game_won = True
        print("WON") #Lets the players know they have won, maybe a little too excited to tell us!
        dx = 0 
        dy = 0 #Stops the momentum of the ball
        ball_color = ball_color_win  #Changes the ball to green when won

    if x > 500: #Conditions for when the ball hits the wall, will cause the ball to go in the opposite direction.
        dx *= -1
    elif x < 0:
        dx *= -1
    elif y > 500:
        dy *= -1
    elif y < 0:
        dy *= -1
    else:
        dx = dx * 0.998
        dy = dy * 0.998 # Makes it that the ball will slow down after being fired, without this it will continously go.
        
    # All drawing code goes here
    screen.fill((0,0,0)) #Background color black
    for i in range(55):
            for j in range(55):
                pygame.draw.circle(screen,(0, 200, 0),(5 + i * 10, j * 10), 3) #Draws the background of the game

    if (aiming == True):
        pygame.draw.circle(screen,(125,125,125),(x,y),50, 5) 

    pygame.draw.circle(screen,ball_color,(x,y),10) #Draws the ball
    pygame.draw.circle(screen,(0,0,0),(goal_X,goal_y),15) #Draws the goal

    pygame.display.update() #Shows the screen